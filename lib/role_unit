### Default environment

#    (c) 2017-2016, n0vember <n0vember@half-9.net>
#
#    This file is part of role_unit.
#
#    role_unit is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    role_unit is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with role_unit.  If not, see <http://www.gnu.org/licenses/>.

### User environment

ru_env_name=${RU_ENV_NAME:-role_unit}
ru_env_image=${RU_ENV_IMAGE:-debian9}
ru_env_type=${RU_ENV_TYPE:-docker}
ru_count=${RU_COUNT:-1}
ru_debug=${RU_DEBUG:-0}
ru_first_debug=0
ru_conf_file_name="ru.conf"

[ -n "$RU_CONF" ] && ru_conf="$RU_CONF"
[ -n "$RU_DEBUG" -a -z "$RU_CONF" ] && ru_first_debug=1

### Global functions

ru_info() {
  local txtcyn='\e[0;36m' # cyan text
  local txtrst='\e[0m'    # text reset
  printf "${txtcyn}"
  echo -n "$@"
  printf "${txtrst}"
  echo
}

ru_usage() {
  local txtred='\e[0;31m' # red test
  local txtrst='\e[0m'    # text reset
  printf "${txtred}" >&2
  echo -n "$@" >&2
  printf "${txtrst}" >&2
  echo >&2
  exit 1
}

### Servers management functions

ru_server() {
  local ru_num=$(ru_server_num $1)
  echo ru_${ru_env_name}_${ru_num}
}

ru_servers() {
  local ru_num
  for ru_num in $(ru_server_nums)
  do
    ru_server ${ru_num}
  done
}

ru_server_nums() {
  local ru_num
  for ru_num in $(seq -w 00 $((ru_count-1)))
  do
    echo ${ru_num}
  done
}

ru_server_num() {
  local ru_num=$(printf "%02d" $1)
  echo ${ru_num}
}

### Ansible management functions

ru_dir_init() {
  if [ ${ru_debug} -eq 0 -o -z "${ru_conf}" ] ; then
    which uuidgen >/dev/null 2>&1 && ru_ansible_id=$(uuidgen)
    which uuid >/dev/null 2>&1 && ru_ansible_id=$(uuid)
    [ -z "${ru_ansible_id}" ] && ru_usage "Unable to get an uuid. Probably neither uuidgen nor uuid is installed."
    ru_ansible_dir=/tmp/ansible/${ru_ansible_id}
    mkdir -p ${ru_ansible_dir}
    ru_conf_file=${ru_ansible_dir}/${ru_conf_file_name}
    echo "ru_ansible_id=${ru_ansible_id}" >> ${ru_conf_file}
    echo "ru_ansible_dir=${ru_ansible_dir}" >> ${ru_conf_file}
  fi
  ru_set_conf_file
  . ${ru_conf_file}
}

ru_ansible_requirements() {
  if [ -f requirements.yml ] ; then
    ru_info fetching requirements
    ansible-galaxy install --force --roles-path ${ru_ansible_dir}/roles/ -r requirements.yml
  fi
}

ru_ansible_init() {
  if [ ${ru_debug} -eq 0 -o -z "${ru_conf}" ] ; then
    echo "ru_ansible_inventory=${ru_ansible_dir}/inventory" >> ${ru_conf_file}
    echo "ru_ansible_playbook=${ru_ansible_dir}/playbook.yml" >> ${ru_conf_file}
    echo "ru_ansible_roledir=${ru_ansible_dir}/roles" >> ${ru_conf_file}
  fi
  . ${ru_conf_file}

  mkdir -p ${ru_ansible_roledir}
  [ ! -L ${ru_ansible_roledir}/${ru_env_name} ] && ln -s $PWD/.. ${ru_ansible_roledir}/${ru_env_name}
  if [ -d ${ru_ansible_dir}/group_vars ] ; then
    rm -r ${ru_ansible_dir}/group_vars
  fi
  if [ ! -L ${ru_ansible_dir}/group_vars ] ; then
    if [ -d group_vars ] ; then
      cp -r $PWD/group_vars ${ru_ansible_dir}/group_vars
    else
      mkdir -p ${ru_ansible_dir}/group_vars
    fi
  fi

  ru_ansible_requirements

  [ ! -f ${ru_ansible_playbook} ] && cat > ${ru_ansible_playbook} << EOF
- hosts: ${ru_env_name}
  roles:
    - ${ru_env_name}
EOF
}

ru_run_ansible_command() {
  local ru_skip_tags
  [ ${ru_debug} -eq 1 -a ${ru_first_debug} -ne 1 ] && ru_skip_tags="--skip-tags ru_once_debug"
  echo "ansible-playbook --verbose --diff -i ${ru_ansible_inventory} --connection=${ru_env_cnx} ${ru_ansible_playbook} ${ru_skip_tags}"
}

ru_run_ansible() {
  if [ $# -gt 0 -a "$1" == "verbose" ] ; then
    ru_info applying ansible playbook
  fi
  assert "$(ru_run_ansible_command)"
}

ru_run_ansible_module() {
  ansible --verbose --diff -i ${ru_ansible_inventory} --connection=${ru_env_cnx} all $@
}

ru_ansible_facts() {
  local ru_server_names=$(ru_server_names)
  [ "$1" == "-n" ] && ru_server_names=$(ru_server_name $2) && shift 2
  ru_server_names=$(echo -e "${ru_server_names}" | tr "\n" ",")
  ansible -i ${ru_ansible_inventory} --connection=${ru_env_cnx} ${ru_server_names} -m setup
}

ru_ansible_fact() {
  local ru_servers
  [ "$1" == "-n" ] && ru_servers="-n $2" && shift 2
  local fact=$1
  ru_ansible_facts ${ru_servers} | grep "\"${fact}\"" | tr -d '" ,' | cut -d ":" -f 2
}

ru_ansible_fact_filter() {
  local ru_servers
  [ "$1" == "-n" ] && ru_server_names=$(ru_server_name $2) && shift 2
  ru_server_names=$(echo -e "${ru_server_names}" | tr "\n" ",")
  local filter=$1
  ansible -i ${ru_ansible_inventory} --connection=${ru_env_cnx} ${ru_server_names} -m setup -a "filter=${filter}"
}

ru_set_conf_file() {
  if [ ${ru_debug} -eq 1 -a -n "${ru_conf}" ] ; then
    ru_conf_file=${ru_conf}
  else
    ru_conf_file=${ru_ansible_dir}/${ru_conf_file_name}
  fi
}

### Set ansible environment

ru_dir_init
export ru_ansible_id ru_ansible_dir ru_conf_file ru_ansible_inventory ru_ansible_playbook ru_ansible_roledir
[ ${ru_debug} -eq 1 -a -n "${ru_conf}" ] && . ${ru_conf_file}

### Test interface

[ -z "$RU_ENV_IMAGE" ] && ru_usage "You did not specify an image on which to run tests."

### Load virtualization specific library

[ -f lib/role_unit_${ru_env_type} ] && . lib/role_unit_${ru_env_type} || ru_usage

# vim: syntax=sh
